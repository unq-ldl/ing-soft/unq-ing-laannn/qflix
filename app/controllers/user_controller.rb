class UserController < ApplicationController

  def show
    @user = User.find(1)
  end

  def buy
    @user.qpoints += params[:qpoints].to_i
    @user.save!
    redirect_to :back
  end


end
