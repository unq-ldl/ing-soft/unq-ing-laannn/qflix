class SeriesController < ApplicationController
  def index
    @series = Serie.all
  end

  def show
    @serie = Serie.find(params[:id])
    @seasons = @serie.seasons
  end

  def play
  end

  def trailer
    @serie = Serie.find(params[:id])
  end

  def search
    @search_text = params[:busqueda]
    @series = Serie.search(@search_text)
  end

  def pay
    @user.qpoints -= params[:qpoints].to_i
    @user.save!
    redirect_to :back
  end

end
