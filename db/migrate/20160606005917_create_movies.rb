class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.string :year
      t.string :synopsis
      t.string :poster
      t.string :genre
      t.integer :stars
      t.integer :price
      t.string :url_movie
      t.string :url_trailer
      t.timestamps null: false
    end
  end
end
