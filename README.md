﻿# QFlix

## Documentación

 * [Documento de Visión y Alcance](https://gitlab.com/unq-ing-laannn/qflix/wikis/doc-vision)
 * [Visual Story Mapping](https://gitlab.com/unq-ing-laannn/qflix/wikis/visual-story-mapping)
 * [Stack Tech](https://gitlab.com/unq-ing-laannn/qflix/wikis/stack-tech)
 * [Iteraciones](https://gitlab.com/unq-ing-laannn/qflix/wikis/iteraciones)
 * [Utilidades](https://gitlab.com/unq-ing-laannn/qflix/wikis/utilidades)
 * [Actividades](https://gitlab.com/unq-ing-laannn/qflix/wikis/actividad)

## Set Up

```sh
$ git clone https://gitlab.com/unq-ing-laannn/qflix.git
$ cd qflix
qflix/ $ gem install bundler  # instala la gema
qflix/ $ bundle install       # instala todas las gemas de Gemfile
qflix/ $ rails server         # levanta un servidor web que apunta a qflix
```

Para verificar que todo funciona ir a `http://localhost:3000`

## Flujo

```sh
./qflix (master) $ git pull origin master # actualiza lo que hay en master
./qflix (master) $ git checkout -b <branch-name> # estaria bueno que sea un short-name de la user story
./qflix (<branch-name>) $ # Trabajo muy duro como un esclavo
./qflix (<branch-name>) $ git add . # en realidad chequear bien a lo que se hace add
./qflix (<branch-name>) $ git commit -m 'mensaje descriptivo de lo realizado'
./qflix (<branch-name>) $ git push origin <branch-name>
```

Si se terminó la funcionalidad, hay que pedir Pull Request desde gitlab:

 * Ir al branch: https://gitlab.com/unq-ing-laannn/qflix/commits/<branch-name>
 * Cliquear el botón verde `+ Create Merge Request`
 * En la pantalla del Merge request completar título y descripción claros y significativos
 * Asignen el merge a LDL
 * Verifiquen que el source branch sea el que están queriendo mergear y que el target branch sea master
 * Cliquear en `Submit merge request`

Si el merge fue aprobado o si tienen que seguir con otra cosa, tienen que volver a la master, hacer pull y volver a crear otro branch

```sh
./qflix (<branch-name>) $ git checkout master
./qflix (master) $ git pull origin master
./qflix (master) $ git checkout -b <branch-name>
...
```

Si usan GUI, el flujo es el mismo, verifiquen bien que estén siguiendo esos pasos

## DB

```sh
$ rake db:create  # Creates databases from config/database.yml
$ rake db:drop    # Drops databases from config/database.yml
$ rake db:migrate # Migrate development database from db/migrate/*.rb files
$ rake db:migrate:reset #  db:drop + db:create + db:migrate
$ rake db:seed    # Load data from db/seeds.rb
```

## Heroku

 1. Crearse una cuenta en Heroku: https://signup.heroku.com/login
 2. Descargar el Toolbelt de Heroku dependiendo del SO que se este utilizando: https://toolbelt.heroku.com/
 3. Instalar el Toolbelt
 4. Solicitar acceso de colaborador a la app
 5. Abrir una consola, y seguir los siguientes pasos:

```sh
$ heroku login   	    # Aca deben ingresar con su mail y password
$ heroku ps:scale web=1 # Se asegura que este corriendo un dyno (containers livianos de Linux)
./qflix $ heroku open	# Posicionados en la ruta del repo, para poder abrir la aplicacion
```

En caso de que un mensaje aparezca diciendo "Permission denied (publickey)", seguir estos pasos:

 1. Abrir una consola de Git Bash
 2. Correr el siguiente comando: `ssh-keygen -t rsa` (cuando lo solicite, dar 3 enters sin especificar nada)
 3. En cualquier otra consola, correr el comando

```sh
$ heroku keys:add
```

Para subir los cambios a Heroku y se actualicen:

```sh
$ git remote add heroku http://git.heroku.com/qflix.git # Esto se realiza solo una vez, para agregar el remote
$ # push to o pull from gitlab
$ heroku login                                          # Hay que estar logueado para pushear
$ git push heroku master                                # Pushear a Heroku
$ heroku run rake db:migrate                            # Actualiza la DB en Heroku (en caso de ser necesario)
$ heroku run rake db:reset                              # Reset & re-run all migrations & seeds (en caso de ser necesario)
$ heroku run rake db:seed                               # Seedea los datos en DB en Heroku (en caso de ser necesario)
```

Otros comandos que pueden servir:

```sh
$ heroku keys	 	       # Lista las keys encontradas para la cuenta
$ heroku apps		       # Lista las apps, sean propias o en colaboracion
$ heroku run rails console # Levantan una consola de rails sobre la que este abierta
```
