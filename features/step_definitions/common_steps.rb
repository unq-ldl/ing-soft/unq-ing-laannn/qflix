# require_relative '../support/seed'

Given(/^I have logged in$/) do
	@user = build(:user)
	@user.save!
	@movie = build(:movie)
	@movie.save!
	steps %Q{
		Given the user "ale" with 9999 QPoints
		When I go home
		Then I should see the username and QPoints in the page
	}
end

Given(/^the user "([^"]*)" with (\d+) QPoints$/) do |username, qpoints|
	@user.username = username
	@user.qpoints  = qpoints
	@user.save!
end

When(/^I go home$/) do
	visit root_path
end

Then(/^I should see the username and QPoints in the page$/) do
	expect(page).to have_content(@user.username)
	expect(page).to have_content("#{@user.qpoints} QP")
end

When(/^I click on "([^"]*)"$/) do |button|
    expect(page).to have_content(button)
    click_on button
end

Then(/^I should see "([^"]*)"$/) do |content|
	expect(page).to have_content(content)
end
