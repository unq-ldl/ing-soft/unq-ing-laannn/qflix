Given(/^the following movies:$/) do |table|
    table.hashes.each do |row|
        movie = build(:movie)
        movie.title  = row[:title]
        movie.stars  = row[:stars]
        movie.poster = row[:poster]
        movie.genre  = row[:genre]
        movie.price  = row[:price]
        movie.save!
    end
end
