Given(/^an authenticated user "([^"]*)"$/) do |user|
  visit root_path
  expect(page).to have_content(user)
end

Given(/^synopsis "([^"]*)"$/) do |synopsis|
  @movie.synopsis = synopsis
  @movie.save!
end

Given(/^movie "([^"]*)"$/) do |url_movie|
  @movie.url_movie = url_movie
  @movie.save!
end

When(/^I click on movies title$/) do
  expect(page).to have_content(@movie.title)
  visit movie_play_path(@movie.id)
end

Then(/^I see the full movie$/) do
  expect(page).to have_selector("iframe[src='https://www.youtube.com/embed/"+@movie.url_movie+"']")
end