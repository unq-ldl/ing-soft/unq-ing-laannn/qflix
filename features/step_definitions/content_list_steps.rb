#encoding: utf-8

Given(/^a movie called "([^"]*)"$/) do |title|
  @movie.title = title
  @movie.save!
end

Given(/^a movie with (\d+) stars$/) do |stars|
  @movie.stars = stars
  @movie.save!
end

Given(/^a movie with poster "([^"]*)"$/) do |poster|
  @movie.poster = poster
  @movie.save!
  end

Given(/^a movie with genre "([^"]*)"$/) do |genre|
  @movie.genre = genre
  @movie.save!
  end

Given(/^a movie that cost (\d+) QPoints$/) do |price|
  @movie.price = price
  @movie.save!
end

When(/^I go to the list of available content$/) do
  visit movies_path
end

Then(/^I should see the title of movie$/) do
  expect(page).to have_content(@movie.title[0, 15])
end

Then(/^I should see the stars qualification of movie$/) do
  expect(page).to have_selector('.movie-'+@movie.id.to_s+' .fa-star', count: @movie.stars)
end

Then(/^I should see the poster of movie$/) do
  expect(page).to have_xpath("//img[contains(@src, \"#{@movie.poster}\")]")
end

Then(/^I should see the genre of movie$/) do
  expect(page).to have_content(@movie.genre)
  end

Then(/^I should see the price of movie$/) do
  expect(page).to have_content(@movie.price)
end
